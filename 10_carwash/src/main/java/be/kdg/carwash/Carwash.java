package be.kdg.carwash;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Bij deze opdracht simuleren we een carwash met
 * één wasstraat waarin zich slechts 1 wagen
 * tegelijk kan bevinden. Je kunt dit vergelijken met
 * een queue waarbij slechts ruimte is voor 1
 * element. Als de wasstraat bezet is moet een
 * wagen die aankomt wachten, is de wasstraat vrij
 * dan kan hij onmiddellijk gewassen worden
 */
public class Carwash {
    private boolean bezet = false;

    /**
     * Als de wasstraat bezet is druk je af welke wagen moet wachten
     *
     * @param wagenNr van de wagen die aankomt
     */
    public void aankomstWagen(int wagenNr) {

    }

    /**
     * De methode vertrekWagen drukt af dat de wagen klaar is.
     *
     * @param wagenNr van de wagen die vertrekt
     */
    public void vertrekWagen(int wagenNr) {

    }
}