package be.kdg.deadlock;

/**
 * https://www.baeldung.com/java-dining-philoshophers
 */
public class Philosopher implements Runnable {

    // The forks on either side of this Philosopher
    private Object firstFork;
    private Object secondFork;

    public Philosopher(Object firstFork, Object secondFork) {
        this.firstFork = firstFork;
        this.secondFork = secondFork;
    }

    private void doAction(String action) throws InterruptedException {
        System.out.printf("%d : %s: %s\n", System.nanoTime(), Thread.currentThread().getName(), action);
        Thread.sleep(((int) (Math.random() * 100)));
    }

    @Override
    public void run() {
        try {
            while (true) {
                // thinking
                doAction("Thinking");
                synchronized (firstFork) {
                    doAction("Picked up first fork");
                    synchronized (secondFork) {
                        // eating
                        doAction("Picked up second fork - eating");
                        doAction("Put down second fork");
                    }
                    // Back to thinking
                    doAction("Put down first fork. Back to thinking");
                }
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            return;
        }
    }
}