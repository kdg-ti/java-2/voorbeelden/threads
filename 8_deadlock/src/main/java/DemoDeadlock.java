import be.kdg.deadlock.Philosopher;

import java.util.Random;
import java.util.Scanner;

/**
 * Mark Goovaerts
 * 17/11/2020
 * <p>
 * https://www.baeldung.com/java-dining-philoshophers
 */
public class DemoDeadlock {
    public static void main(String[] args) {
        Philosopher[] philosophers = new Philosopher[5];
        Object[] forks = new Object[5];
        Thread[] threads = new Thread[5];

        for (int i = 0; i < forks.length; i++) {
            forks[i] = new Object();
        }

        for (int i = 0; i < philosophers.length; i++) {
            Object leftFork = forks[i];
            Object rightFork = forks[(i + 1) % forks.length];

            philosophers[i] = new Philosopher(leftFork, rightFork);
            threads[i] = new Thread(philosophers[i], "Philosopher " + (i + 1));
            threads[i].start();
        }
    }
}

