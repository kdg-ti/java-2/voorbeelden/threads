import be.kdg.guarded.Consumer;
import be.kdg.guarded.Drop;
import be.kdg.guarded.Producer;

public class DemoGuardedBlock {
    public static void main(String[] args) {
        Drop drop = new Drop();
        Thread producer = new Thread(new Producer(drop));
        Thread consumer = new Thread(new Consumer(drop));
        producer.start();
        consumer.start();
    }
}
