package be.kdg.guarded;


public class Drop {
    private String message; // sent from producer to consumer.
    private boolean empty = true;

    public synchronized String take() {
        while (empty) { //Wait until message is available.
            try {
                wait();
            } catch (InterruptedException e) {
                // empty
            }
        }
        empty = true; //Toggle status.
        notifyAll(); //Notify producer that status has changed.
        return message;
    }

    public synchronized void put(String message) {
        while (!empty) { //Wait until message has been retrieved.
            try {
                wait();
            } catch (InterruptedException e) {
                // empty
            }
        }
        empty = false; //Toggle status.
        this.message = message; //Store message.
        notifyAll(); //Notify consumer that status has changed.
    }
}
